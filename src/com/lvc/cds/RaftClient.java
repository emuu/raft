package com.lvc.cds;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Message;
import com.google.protobuf.util.JsonFormat;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class RaftClient {
    public static final int portNum = 6969;

    public static void main(String[] args) {
        Socket sock;
        ArrayList<String> ips = new ArrayList<>();
        try {
            Path basePath = Paths.get(".").toAbsolutePath();
            Path filePath = Paths.get("src/com/lvc/cds/IPs");
            BufferedReader reader = Files.newBufferedReader(Paths.get(basePath.toString(), filePath.toString()).normalize(), StandardCharsets.UTF_8);
            String line;
            while ((line = reader.readLine()) != null) {
                if (!line.equals(InetAddress.getLocalHost().getHostAddress()))
                    ips.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        while (true) {
                Scanner in = new Scanner(System.in);
                System.out.println("Enter a Command: ");
                String command = in.next();
                System.out.println("Enter Key :");
                String key = in.next();
                System.out.println("Enter Value:");
                String value = in.next();


                for (String ipAdd : ips) {
                    try {
                        sock = new Socket(ipAdd, portNum);
                        while (command != null && key != null && value != null) {
                            RaftProto.ClientMessage.Builder clientMessage = RaftProto.ClientMessage.newBuilder();
                            clientMessage.setCommand(command);
                            clientMessage.setKey(key);
                            clientMessage.setValue(value);
                            clientMessage.setIp(InetAddress.getLocalHost().toString());
                            try {
                                JSONParser parser = new JSONParser();
                                Message toSend = clientMessage.build();
                                JSONObject obj = (JSONObject) parser.parse(JsonFormat.printer().print(toSend));
                                //System.out.println("Sent: " + obj.toString());
                                DataOutputStream out = new DataOutputStream(sock.getOutputStream());
                                out.writeInt(obj.toString().getBytes().length);
                                out.write(obj.toString().getBytes(StandardCharsets.UTF_8));
                                Socket finalSock = sock;
                                Thread t = new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        String result = "";
                                        DataInputStream input = null;
                                        try {
                                            input = new DataInputStream(finalSock.getInputStream());
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        int length = 0;
                                        try {
                                            length = input.readInt();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        if (length > 0) {
                                            byte[] message = new byte[length];
                                            try {
                                                input.readFully(message, 0, message.length);
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                                System.out.println(length);
                                            }
                                            result = new String(message);
                                            System.out.println(result);
                                        }
                                    }
                                });
                                t.start();

                            } catch (InvalidProtocolBufferException | ParseException e) {
                            }


                        }
                    } catch (IOException e) {
                        e.printStackTrace();

                    }


                }
            System.out.println("Would you like to enter another Command? :");
            String yesOrNo = in.next();
            if(yesOrNo.equalsIgnoreCase("No")) {
                break;
            }
            if(!yesOrNo.equalsIgnoreCase("yes")){
                System.out.println("Invalid Submission : please enter [yes] or [no]");
                yesOrNo = in.next();

            }


        }

    }
}
