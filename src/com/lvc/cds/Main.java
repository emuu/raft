package com.lvc.cds;


import java.io.File;
import java.io.IOException;

import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;


public class Main {
    public static void main(String[] args) throws InterruptedException, ClassNotFoundException, IOException {
        RaftNode node = new RaftNode();
        if(args.length != 0 && args[0].equals("--rebuild")){
            if (args.length > 1) {
                node.rebuild(args[1]);
            } else {
                System.out.print("Please input a filepath: ");
                Scanner in = new Scanner(System.in);
                String file = in.nextLine();
            }

        }
        else {
            Path basePath = Paths.get(".").toAbsolutePath();
            Path filePath = Paths.get("src/logData.txt");
            File file = new File(String.valueOf(Paths.get(basePath.toString(),filePath.toString()).normalize()));
            PrintWriter writer = new PrintWriter(file);
            writer.print("");
            writer.close();
        }
        node.runNode();
    }
}
