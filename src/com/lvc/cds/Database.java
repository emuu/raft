package com.lvc.cds;
import java.io.*;
import java.util.ArrayList;



public class Database implements Serializable {
//    private static ConcurrentHashMap<String, String> database = new ConcurrentHashMap<>();
    private static ArrayList<RaftNode.Command> logs = new ArrayList<>();
     /*
      Method to storeLogs to a file
      */
     public static void storeLogs(ArrayList<RaftNode.Command> log) throws IOException {
        File file = new File("logData.txt");
         FileOutputStream out = new FileOutputStream(file);
         ObjectOutputStream os = new ObjectOutputStream(out);
         os.writeObject(log);
         os.close();
     }
     /*
      When we want to start a fresh node run this to delete file
      */
     public static void clearFile(){
         logs.clear();
     }
     /*
       Load in Log File
      */
     public static ArrayList<RaftNode.Command> loadLogs(String fileName) throws IOException, ClassNotFoundException {
         File file = new File(fileName);
         if(file.exists() && file.length() > 0 ) {
             FileInputStream in = new FileInputStream(file);
             ObjectInputStream inS = new ObjectInputStream(in);
             logs = (ArrayList<RaftNode.Command>) inS.readObject();
             inS.close();
             return logs;
         }
         else
             return logs;
     }
    /*
    Store Data in HashMap
     */
//    public static void addData(RaftNode.Command c){
//        database.put(c.key, c.value);
//    }
//    /*
//    Delete data
//     */
//    public  static void deleteDataPoint(RaftNode.Command c){
//        database.remove(c.key);
//    }

    /*
    Get Database
     */
//    public static ConcurrentHashMap<String, String> getDatabase(){
//        return database;
//    }


    //Load in log Array
    //Write Log Array to File
    //Store Hashmap of key, value pairs
     /*
     Test Database Storage
      */
//    public static void main(String[] args) throws IOException, ClassNotFoundException {
//        RaftNode.Command c = new RaftNode.Command(1,"WRITE","Hello","World");
////        logs.add(c);
////        storeLogs(logs);
//       logs = loadLogs();
//        System.out.println(logs.size());
//    }
}
