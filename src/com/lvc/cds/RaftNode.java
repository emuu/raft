 package com.lvc.cds;

import com.google.protobuf.Message;

import com.lvc.cds.RaftProto.AppendEntries;
import com.lvc.cds.RaftProto.RequestVote;
import com.lvc.cds.RaftProto.RequestVoteResponse;
import com.lvc.cds.RaftProto.AppendEntriesResponse;
import com.lvc.cds.RaftProto.ClientMessage;

import java.io.*;
import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

 public class RaftNode {
    public enum State {FOLLOWER, LEADER, CANDIDATE}

    // Persistent State
    private int currentTerm = 0;
    private String votedFor = null;
    private int votesFor = 0;
    private String id;
    private String leaderId;
    private Network network;
    long lastMessage;

    ArrayList<Command> log;
    ConcurrentHashMap<String, String> database;

    Network.ClientThread client;

    // Volatile State
    private int commitIndex = 0;
    private int lastApplied = 0;

    public State leader() {
        // Leader State
        int[] nextIndex;
        int[] matchIndex;

        network.sendAppendEntries(currentTerm, new ArrayList<Command>());

        ArrayList<Command> commands = new ArrayList<>();

        client = network.client();
        client.start();
        //client.kill();

        lastMessage = System.currentTimeMillis();
        while(true){
            /*
            if commitIndex > lastApplied
              commit log[lastApplied]
              lastApplied++
              ------------------------
              check for incoming cluster messages
               - check term. Revert if necessary
                "Can only be new AppendEntries and RequestVotes with larger term"
               -otherwise,
               [How to handle event where a response to AppendEntries has lower term ]
             */
            double timeout = 10000;
            if(lastMessage + timeout < System.currentTimeMillis()){
                network.sendAppendEntries(currentTerm, commands);
                commands.clear();
                lastMessage = System.currentTimeMillis();
            }

            Message mess = network.checkIncomingMessage();
            if(mess != null) {
                switch (mess.getDescriptorForType().getName()) {
                    case "AppendEntries":
                        AppendEntries aeMessage = (AppendEntries) mess;
                        if (aeMessage.getTerm() >= currentTerm)
                            return State.FOLLOWER;
                        client.kill();
                        network.getIncomingMessage();
                        break;
                    case "RequestVote":
                        RequestVote rvMessage = (RequestVote) mess;
                        if (rvMessage.getTerm() > currentTerm)
                            return State.FOLLOWER;
                        client.kill();
                        network.getIncomingMessage();
                        break;
                    case "AppendEntriesResponse":
                        AppendEntriesResponse aerMessage = (AppendEntriesResponse) network.getIncomingMessage();
                        break;
                    case "ClientMessage":
                        ClientMessage clientMessage = (ClientMessage) network.getIncomingMessage();
                        commands.add(new Command(currentTerm, clientMessage.getCommand(), clientMessage.getKey(), clientMessage.getValue(), clientMessage.getIp()));
                        break;
                    default:
                        network.getIncomingMessage();
                }
            }
        }
    }

    public State candidate() throws InterruptedException {
        while(true) {
            double timeout = 15000;
            Message mess = network.checkIncomingMessage();
            if (mess == null) {
                if (lastMessage + timeout < System.currentTimeMillis()) {
                    Thread.sleep((long) Math.random() * 5000);
                    currentTerm++;
                    votedFor = id;
                    votesFor = 1;
                    lastMessage = System.currentTimeMillis();
                    network.sendRequestVote(currentTerm);
                    return State.CANDIDATE;
                }
            } else {
                switch (mess.getDescriptorForType().getName()) {
                    case "AppendEntries":
                        AppendEntries aeMessage = (AppendEntries) mess;
                        if (aeMessage.getTerm() >= currentTerm)
                            return State.FOLLOWER;
                        network.getIncomingMessage();
                        break;
                    case "RequestVote":
                        RequestVote rvMessage = (RequestVote) mess;
                        if(rvMessage.getTerm() > currentTerm)
                            return State.FOLLOWER;
                        network.getIncomingMessage();
                        break;
                    case "RequestVoteResponse":
                        RequestVoteResponse rvrMessage = (RequestVoteResponse) network.getIncomingMessage();
                        if (rvrMessage.getVoteGranted())
                            votesFor++;
                        if (votesFor > network.getMajority())
                            return State.LEADER;
                    default:
                        network.getIncomingMessage();
                }
            }
        }
    }

    public State follower() throws InterruptedException, IOException {
        while(true) {
            double timeout = 20000;
            Message mess = network.getIncomingMessage();
            if (mess == null) {
                if (lastMessage + timeout < System.currentTimeMillis()) {
                    Thread.sleep((long) Math.random() * 5000);
                    currentTerm++;
                    votedFor = id;
                    votesFor = 1;
                    lastMessage = System.currentTimeMillis();
                    network.sendRequestVote(currentTerm);
                    return State.CANDIDATE;
                }
            } else {
                switch (mess.getDescriptorForType().getName()) {
                    case "AppendEntries":
                        //Update DataBase
                        AppendEntries aeMessage = (AppendEntries) mess;
                        if(aeMessage.getTerm() < currentTerm ||
                                log.get(aeMessage.getPreviousLogIndex()).term != aeMessage.getPreviousLogTerm()) {
                            network.sendAppendEntriesResponse(currentTerm, false);
                            break;
                        }
                        int index = aeMessage.getPreviousLogIndex();
                        for(AppendEntries.Pair entry : aeMessage.getEntryList()){
//                            index++;
                            try{
                                if(log.get(index).term != entry.getTerm()){
                                    log.subList(index, log.size()).clear();
                                    break;
                                }
                            }
                            catch(ArrayIndexOutOfBoundsException e){ }
                        }
                        for(AppendEntries.Pair entry : aeMessage.getEntryList()){
                            log.add(new Command(aeMessage.getTerm(), entry.getCommand().toString(), entry.getKey(), entry.getValue()));
                        }
                        currentTerm = aeMessage.getTerm();
                        network.sendAppendEntriesResponse(currentTerm, true);
                        Database.storeLogs(log);
                        lastMessage = System.currentTimeMillis();

                        break;
                    case "RequestVote":
                        RequestVote rvMessage = (RequestVote) mess;
                        if( rvMessage.getTerm() < currentTerm ||
                                !(votedFor == null || votedFor.equals(id) || votedFor.equals(rvMessage.getCandidateId()))||
                            log.get(log.size() - 1).term > rvMessage.getLastLogTerm())
                            network.sendRequestVoteResponse(currentTerm, false);
                        else {
                            network.sendRequestVoteResponse(currentTerm, true);
                            votedFor = rvMessage.getCandidateId();
                            lastMessage = System.currentTimeMillis();
                        }
                        break;
                }
            }
        }
    }

    public void rebuild(String filePath) throws IOException, ClassNotFoundException {
      log = Database.loadLogs(filePath);
    }

    public void runNode() throws InterruptedException, IOException {
        State currentState = State.FOLLOWER;
        database = new ConcurrentHashMap<>();
        lastMessage = System.currentTimeMillis();
        try {
            id = Inet4Address.getLocalHost().getHostAddress();
        }
        catch(UnknownHostException e){ }

        log = new ArrayList<>();
        network = new Network(this);

        log.add(new Command(currentTerm, AppendEntries.Command.READ.toString(), "", ""));

        while(true){
            if(commitIndex > lastApplied){
                lastApplied++;

                 Command command = log.get(lastApplied);
                  switch(command.command.toString().toUpperCase()) {
                      case "WRITE":
                          database.put(command.key, command.value);
                          break;
                      case "DELETE":
                          database.remove(command.key);
                          break;
                  }
            }
            switch(currentState){
                case FOLLOWER:
                    System.out.println("I am a follower");
                    currentState = follower();
                    break;
                case LEADER:
                    System.out.println("I am a leader");
                    leaderId = id;
                    currentState = leader();
                    break;
                case CANDIDATE:
                    System.out.println("I am a candidate");
                    currentState = candidate();
                    break;
            }
        }
    }

    static class Command implements Serializable {
        AppendEntries.Command command;
        String key, value;
        int term;
        String clientIp;

        public Command(int term, String command, String key, String value, String ip){
            this.term = term;
            switch(command.toUpperCase()){
                case "WRITE":
                    this.command = AppendEntries.Command.WRITE;
                    break;
                case "DELETE":
                    this.command = AppendEntries.Command.DELETE;
                case "READ":
                    this.command = AppendEntries.Command.READ;
            }
            this.key = key;
            this.value = value;
            this.clientIp = ip;
        }

        public Command(int term, String command, String key, String value){
            this(term, command, key, value, null);
        }
    }

    class Pair{
        int term;
        String key, value;
        public Pair(int term, String key, String value){
            this.term = term;
            this.key = key;
            this.value = value;
        }
    }

    //<editor-fold desc="Getters">
    public int getCurrentTerm() {
        return currentTerm;
    }

    public String getVotedFor() {
        return votedFor;
    }

    public String getId() {
        return id;
    }

    public int getCommitIndex() {
        return commitIndex;
    }

    public int getLastApplied() {
        return lastApplied;
    }

    public String getLeaderId() {
        return leaderId;
    }

    public ArrayList<Command> getLog() {
        return log;
    }
    //</editor-fold>
}
