package com.lvc.cds;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Message;

import com.lvc.cds.RaftProto.AppendEntries;
import com.lvc.cds.RaftProto.RequestVote;
import com.lvc.cds.RaftProto.AppendEntriesResponse;
import com.lvc.cds.RaftProto.RequestVoteResponse;
import com.lvc.cds.RaftProto.ClientMessage;

import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.LinkedBlockingQueue;

import com.google.protobuf.util.JsonFormat;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Network {
    private ConcurrentLinkedQueue<Message> inbound;
    private BlockingQueue<Message> outbound;

    ArrayList<String> ips;
    int port = 8080;
    int leaderPort = 6969;

    RaftNode node;

    public double getMajority(){
        return (ips.size() / 2);
    }

    public Network(RaftNode node){
        // Get IP list
        ips = new ArrayList<>();

        inbound = new ConcurrentLinkedQueue<>();
        outbound = new LinkedBlockingQueue<Message>();

        incoming();
        outgoing();

        try {
            Path basePath = Paths.get(".").toAbsolutePath();
            Path filePath = Paths.get("src/com/lvc/cds/IPs");
            BufferedReader reader = Files.newBufferedReader(Paths.get(basePath.toString(), filePath.toString()).normalize(), StandardCharsets.UTF_8);
            String line;
            while((line = reader.readLine()) != null){
                if(!line.equals(InetAddress.getLocalHost().getHostAddress()))
                    ips.add(line);
            }
        }catch (IOException e) {
            e.printStackTrace();
        }

        this.node = node;
    }

    public void sendAppendEntries(int term, ArrayList<RaftNode.Command> commands) {
        // assemble a Message object.
        // put it in the queue, return immediately

        AppendEntries.Builder message = AppendEntries.newBuilder();
        message.setType("AppendEntries");
        message.setTerm(term);
        message.setLeaderId(node.getLeaderId());
        message.setPreviousLogIndex(node.getLastApplied());
        message.setPreviousLogTerm(node.getLog().get(node.getLastApplied()).term);
        for(RaftNode.Command command : commands){
            message.addEntry(
                    AppendEntries.Pair.newBuilder()
                        .setTerm(term)
                        .setCommand(command.command)
                        .setKey(command.key)
                        .setValue(command.value)
                        .build()
            );
        }
        message.setLeaderCommit(node.getCommitIndex());
        AppendEntries m = message.build();
        if(m != null)
            outbound.add(m);
        else
            System.out.println("Null in Append Entries");
    }

    public void sendRequestVote(int term) {
        // assemble a Message object.
        // put it in the queue, return immediately

        RequestVote.Builder message = RequestVote.newBuilder();
        message.setType("RequestVote");
        message.setTerm(term);
        message.setCandidateId(node.getId());
        message.setLastLogIndex(node.getLastApplied());
        try {
            message.setLastLogTerm(node.getLog().get(node.getLastApplied()).term);
        }catch(IndexOutOfBoundsException e){}
        RequestVote m = message.build();
        if(m != null)
            outbound.add(m);
        else
            System.out.println("Null in Request Vote");

    }

    public void sendRequestVoteResponse(int term, boolean vote) {
        // assemble a Message object.
        // put it in the queue, return immediately

        RequestVoteResponse.Builder message = RequestVoteResponse.newBuilder();
        message.setType("RequestVoteResponse");
        message.setTerm(term);
        message.setVoteGranted(vote);
        RequestVoteResponse m = message.build();
        if(m != null)
            outbound.add(m);
        else
            System.out.println("Null in Request Vote Response");
    }

    public void sendAppendEntriesResponse(int term, boolean success) {
        // assemble a Message object.
        // put it in the queue, return immediately

        AppendEntriesResponse.Builder message = AppendEntriesResponse.newBuilder();
        message.setType("AppendEntriesReply");
        message.setTerm(term);
        message.setSuccess(success);
        AppendEntriesResponse m = message.build();
        if(m != null)
            outbound.add(message.build());
        else
            System.out.println("Null in Append Entries Response");
    }

    public Message getIncomingMessage() {
        // pop something off the incoming queue, return it.
        return inbound.poll();
    }

    public Message checkIncomingMessage(){
        if(inbound.size() > 0)
            return inbound.peek();
        return null;
    }

    private void outgoing() {
        // some setup code.
        Thread th = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        JSONParser parser = new JSONParser();
                        Message toSend = outbound.take();
                        JSONObject obj = (JSONObject) parser.parse(JsonFormat.printer().print(toSend));
                        System.out.println("Sent: " + System.currentTimeMillis() + " " + obj.toString());
                        for(String ip : ips){
                            try (
                                    Socket socket = new Socket();
                                    ) {
                                socket.connect(new InetSocketAddress(ip, port), 1000);
                                OutputStream o = socket.getOutputStream();
                                DataOutputStream out = new DataOutputStream(o);
                                out.writeInt(obj.toString().getBytes().length);
                                out.write(obj.toString().getBytes(StandardCharsets.UTF_8));
                                out.flush();
                            }catch(IOException e){
                                System.out.println("ERROR HERE");
                            }
                        }
                    }
                    catch(InterruptedException | InvalidProtocolBufferException | ParseException e){
                        System.out.println("ERROR THERE");
                    }
                }
            }
        });
        th.start();
    }

    private void incoming() {
        // some setup code
        Thread th = new Thread(new Runnable() {
            @Override
            public void run() {
                // make a listening socket
                ServerSocket sock = null;
                try {
                    sock = new ServerSocket(port);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                while (true) {
                    // accept a connection

                    // handle the connection by reading the byte
                    // stream, converting to a protobuf message,
                    // converting to some other object (a Message
                    // again?), then pushing onto the queue.
                    try {
                        Socket socket = sock.accept();

                        String result = "";
                        DataInputStream in = new DataInputStream(socket.getInputStream());
                        int length = in.readInt();
                        if (length > 0) {
                            byte[] message = new byte[length];
                            in.readFully(message, 0, message.length);
                            result = new String(message);
                        }

                        if (!result.toString().equals("")) {
                            JSONParser parser = new JSONParser();
                            JSONObject obj = (JSONObject) parser.parse(result.toString());

                            System.out.println("Incomming: " + System.currentTimeMillis() + " " + obj.get("type").toString() + obj.toString());
                            switch (obj.get("type").toString()) {
                                case "AppendEntries":
                                    AppendEntries.Builder ae = AppendEntries.newBuilder();
                                    JsonFormat.parser().merge(obj.toString(), ae);
                                    inbound.add(ae.build());
                                    if(!ae.build().getEntryList().isEmpty()){
                                        int x = 5 + 2;
                                    }
                                    break;
                                case "RequestVote":
                                    RequestVote.Builder rv = RequestVote.newBuilder();
                                    JsonFormat.parser().merge(obj.toString(), rv);
                                    inbound.add(rv.build());
                                    break;
                                case "AppendEntriesResponse":
                                    AppendEntriesResponse.Builder aer = AppendEntriesResponse.newBuilder();
                                    JsonFormat.parser().merge(obj.toString(), aer);
                                    inbound.add(aer.build());
                                    break;
                                case "RequestVoteResponse":
                                    RequestVoteResponse.Builder rvr = RequestVoteResponse.newBuilder();
                                    JsonFormat.parser().merge(obj.toString(), rvr);
                                    inbound.add(rvr.build());
                                    break;
                            }
                        }
                    } catch (IOException | ParseException e) {
//                        System.out.println("In");
                    }
                }
            }
        });
        th.start();
    }

    public static class ClientThread extends Thread {
        protected ServerSocket leadSock;

        public void kill() {
            try {
                leadSock.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public ClientThread client(){
        ClientThread th = new ClientThread() {
            @Override
            public void run() {
                try {
                    leadSock = new ServerSocket(leaderPort);
                    // Client communication
                    while(true) {
                        if(!node.getId().equals(node.getLeaderId()))
                            break;
                        try {
                            Socket socket = leadSock.accept();

                            if(!node.getId().equals(node.getLeaderId()))
                                break;

                            String result = "";
                            DataInputStream in = new DataInputStream(socket.getInputStream());
                            int length = in.readInt();

                            if (length > 0) {
                                byte[] message = new byte[length];
                                in.readFully(message, 0, message.length);
                                result = new String(message);
                            }

                            JSONParser parser = new JSONParser();
                            JSONObject obj = (JSONObject) parser.parse(result.toString());
                            ClientMessage.Builder message = ClientMessage.newBuilder();
                            JsonFormat.parser().merge(obj.toString(), message);
                            inbound.add(message.build());
                        } catch (IOException | NullPointerException | ParseException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        return th;
    }
}
